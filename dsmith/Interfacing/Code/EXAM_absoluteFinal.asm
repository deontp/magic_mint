; Hardware Interfacing Practical Examination 2016
; Authors: Dylan Smith, Paul Sebeikin, Deon Pearson

.include "m16def.inc"

; save some temporary registers
.def TMP1=R16
.def TMP2=R17
.def TMP3=R18

.def RXPOS=R19							    ; register to keep track of the position of what was received

.def ZERO=R20								; register just to store 0

.def MASK=R21								; mask to drive the stepper

.def COUNT=R22								; counter value used to keep track of steps and characters written to LCD

; acts as a status register: keeps track of direction of motor and whether the menu was sent
.def MOT_STAT=R23							; bit setup: 0=direction(1=cw, 0=acw), 1=on/off(1 on 0 off), 2=menu sent (1 sent 0 not sent)


.def TMP4=R24								; used to store ADC value

; registers used for delay routine
.def LOOP1=R25
.def LOOP2=R26
.def LOOP3=R27

.eseg
.org 0x000
; define the messages stored in EEPROM
EE_LOOKUP: .db "Message 1", 0x00, "Message 2", 0x00, "Message 3 ABCDEFGHI", 0x00,0x00


; allocate the space in RAM to store the messages
.dseg
RXBUFFER: .byte 10 							; reserve 10 bytes for receive buffer

; reserve space for messages stored in EEPROM 
MESSAGE_1: .byte 20 						
MESSAGE_2: .byte 20
MESSAGE_3: .byte 20

.cseg
.org $000									; locate code at address $000
jmp START									; Jump to the START Label

; define interrupt vectors
.org URXCaddr
jmp URXC_ISR
.org UTXCaddr
jmp UTXC_ISR
.org OVF0addr
jmp OFV0_ISR
.org ADCCaddr
rjmp ADC_ISR

.org $02A									; locate code past the interupt vectors


; start up routine
START:
	; initialise the stack pointer
	LDI TMP1, LOW(RAMEND)	                
	OUT SPL, TMP1
	LDI TMP1, HIGH(RAMEND)
	OUT SPH, TMP1

; initilise ZERO register
	LDI TMP1, 0x00
	MOV ZERO, TMP1
	MOV RXPOS, ZERO
; initilise UART
	CALL INIT_UART
; initilise ports 
	CALL SETUP_PORTS
; initilise stepper motor
	CALL SETUP_MOTOR
; move EEPROM messages to RAM
	CALL STORE_IN_RAM
; send the menu to serial
	CALL SEND_MENU

; enable global interrupts
	SEI

MAIN_LOOP:
	NOP
	NOP
	RJMP MAIN_LOOP

SETUP_PORTS:
	LDI TMP1, 0b11111111
	OUT DDRA, TMP1								; set all bits in PORTA to output mode.
	LDI TMP1, 0b00000001
	OUT PORTA, TMP1								; all LED's initially off (PA0 reserved for VAR)
	RET

SETUP_MOTOR:
	LDI TMP1, 0b11110000						; initilise PA4:7 to outputs to drive stepper
	IN TMP2, DDRD
	OR TMP2, TMP1								; check if stepper is enabled (on)
	OUT DDRD, TMP2								; setting PD
	LDI MASK, 0b10000000		
	OUT PORTD, MASK
	ORI MOT_STAT, 0b00000010					; set the motor status to on
	RET

; setting the timer over flow to turn the motor 180 degrees with prescaler of 256
SETUP_TIMER_0:			
	LDI TMP1, 0b00000001
	OUT TIMSK, TMP1								; enable overflow interrupt (p 85)
	MOV TMP1, ZERO
	OUT TCNT0, TMP1
	LDI TMP1, 0b00000100						; start the timer with a prescaler of 256 (p 85)
	OUT TCCR0, TMP1								
	RET

; routine that sends the menu onto the UART
SEND_MENU:
    ; Z is reserved for pointing to next char
	; Z register is used specifically for program memory because LPM instruction
	; only works with Z register.
    LDI ZL, LOW(2*TEXT_MENU)					; point z lo to beginning of menu		
    LDI ZH, HIGH(2*TEXT_MENU)
	LPM TMP1, Z+

	OUT UDR, TMP1								; send value (P in this case) out onto UART
	RET

INIT_UART:
;set baud rate (9600,8,n,2)
 	LDI TMP1, 51								; p 173
	OUT UBRRH, ZERO								; set to 0 beause we don't use the upper 8 bits
	OUT	UBRRL, TMP1								; set to 51 for the UART config
; set rx and tx enable
	SBI UCSRB, RXEN
	SBI UCSRB, TXEN
; enable uart interrupts
	SBI UCSRB, RXCIE
    SBI UCSRB, TXCIE
	RET

; delay routine used for ADC PWM simulation
; delay routine scales with ADC value
DEL :   	LDI LOOP1, 0x01
DEL1 :    	LDI LOOP2, 0b00000111
DEL2 :    	MOV LOOP3, TMP4						; the ADC value
DEL3 :    	DEC LOOP3
			BRNE  DEL3
			DEC LOOP2
			BRNE  DEL2
			DEC LOOP1
			BRNE  DEL1
			RET

; Code for transmit complete - this will transmit the rest of the menu 
;		after the P is transmitted
UTXC_ISR:
	LPM TMP1, Z+								; fetch next value
	CPI TMP1, 0x00	 							; if null found
	BREQ EXIT_TX_ISR							;  	exit the ISR
	OUT UDR, TMP1								; else output to uart
	RETI

; no more characters (null found) so exit
EXIT_TX_ISR:
	CBI UCSRB, TXCIE							; transmit complete interrupt disabled
	RETI

; Code for receive complete
URXC_ISR:
    IN TMP1, UDR
    LDI XL,LOW(RXBUFFER)				 		; acquires the position in RAM where message to be stored
    LDI XH,HIGH(RXBUFFER)
	ADD XL, RXPOS
	ADC XH, ZERO
    ST X, TMP1									; put into RAM
    CPI TMP1, '.'								; found a . so must be a command
	BREQ PARSE_MESSAGE				
	INC RXPOS
	RETI

; T/C0 overflow ISR
OFV0_ISR:
	; spills registers to stack
	PUSH TMP1
	PUSH TMP2
	IN TMP1, SREG
	PUSH TMP1

	CPI COUNT, 100								; perform 100 steps to turn 180
	BREQ STOP_TIMER_0							; If 100 performed, stop the timer
   	RCALL TURN_MOTOR							; else carry on turning
	INC COUNT

	; restore from the stack
	POP TMP1
	OUT SREG, TMP1
	POP TMP2
	POP TMP1
	RETI

; kill timer once reached COUNT
STOP_TIMER_0:
	OUT TCCR0, ZERO								; stop timer
	OUT TIMSK, ZERO								; disable the timer interrupt

	; restore from the stack
	POP TMP1
	OUT SREG, TMP1
	POP TMP2
	POP TMP1
	RETI

; ADC interrupt
ADC_ISR:
	; spill registers to stack
	PUSH TMP1
	PUSH TMP2
	IN TMP1, SREG
	PUSH TMP1

	; perform conversion to 5 bits
	IN TMP1, ADCH								; get ADC value
	LSR TMP1									; dividing by 32, so do right shift 3 times (bitwise division)
	LSR TMP1
	LSR TMP1
	MOV TMP4, TMP1								; use the number from the ADC in the delay, so save it to a temp register
	LSL TMP1									; shift left, since we don't use PA:0 (connected to VAR)

	; output results
	OUT PORTA, TMP1								; output ADC value
	CALL del									; call a delay to simulate PWM in software
	LDI TMP1, 0x00								; initial back to 0
	OUT PORTA, TMP1

	; restore registers
	POP TMP1
	OUT SREG, TMP1
	POP TMP2
	POP TMP1

	RETI

; decide what task to do..
PARSE_MESSAGE:
    LDI XL, LOW(RXBUFFER)
    LDI XH, HIGH(RXBUFFER)
	LD TMP1, X+									; increment X to point to the next byte to check if we are working with double digits
	CPI TMP1, '1'
	BREQ TASK_1
	CPI TMP1, '2'
	BREQ TASK_2
	CPI TMP1, '3'
	BREQ TASK_3
	CPI TMP1, '4'
	BREQ TASK_4
	CPI TMP1, '5'
	BREQ TASK_5
	CPI TMP1, '6'
	BREQ TASK_6
	CPI TMP1, '7'
	BREQ TASK_7
	CPI TMP1, '8'
	BREQ TASK_8
	CPI TMP1, '9'
	BREQ TASK_9

EXIT_PARSE:
    MOV RXPOS, ZERO
	RETI

; single step clockwise
TASK_1:																		
	LD TMP1, X 									; fetch next value
	CPI TMP1, '0'								; if ANOTHER 0 is found..
	BREQ TASK_10 								;	go to TASK_10
	CPI TMP1, '1' 								; else if ANOTHER 1 is found...
	BREQ TASK_11 								;	go to TASK_11
	ORI MOT_STAT, 0b00000001					; else not another digit, so must be 1. Set direction to clockwise
	RCALL TURN_MOTOR							; go to TURN_MOTOR routine
	RJMP EXIT_PARSE

; single step anti-clockwise
TASK_2:											
	ANDI MOT_STAT, 0b11111110					; set direction to anti-clockwise
	RCALL TURN_MOTOR
	RJMP EXIT_PARSE

; turn 180 clockwise
TASK_3:											
	SBRS MOT_STAT, 1							; if the second bit in mot_stat is set it is on so do move
	RJMP EXIT_PARSE								; motor is off, so dont move, and exit
	CALL SEND_MENU								; resend menu once task is complete
	MOV COUNT, ZERO								; reset count
	ORI MOT_STAT, 0b00000001					; set direction to clockwise
	CALL SETUP_TIMER_0							; start the timer
	RJMP EXIT_PARSE

; turn 180 anti-clockwise
TASK_4:											
	SBRS MOT_STAT, 1							; if the second bit in mot_stat is set it is on so do move
	RJMP EXIT_PARSE								; motor is off, so dont move, and exit
	CALL SEND_MENU								; resend menu once task is complete
	MOV COUNT, ZERO 							; reset count
	ANDI MOT_STAT, 0b11111110					; set direction to anti-clockwise
	CALL SETUP_TIMER_0 							; start the timer
	RJMP EXIT_PARSE

; disable stepper motor
TASK_5:											
	ANDI MOT_STAT, 0b11111101					; turn stepper motor off by clearing bit 1 in MOT_STAT
	LDI MASK, 0x00 								; set mask to 0
	OUT PORTD, MASK 							; output on port D
	RJMP EXIT_PARSE

; enable stepper motor
TASK_6:											
	ORI MOT_STAT, 0b00000010 					; set stepper motor to on MOT_STAT reg
	LDI MASK, 0b10000000 						; reset back
	OUT PORTD, MASK 							; output on port D
	RJMP EXIT_PARSE 

; start ADC PWM	
TASK_7:											
	LDI TMP1, 0b01100000 						; ADC0 (p222) since PA0 is connected to VAR				
	OUT ADMUX, TMP1 							; we write 1 to ADLAR in ADMUX to left adjust
	LDI TMP1, 0b11111111 						; enable ADC
	OUT ADCSRA, TMP1 							; Starts in single conversion, ADC interrupt, flag. interrupt enable
												; 	prescaler of 128 (p 223)
	RJMP EXIT_PARSE

; disables the ADC
TASK_8:
	CBI ADCSRA, 7								; clear bit 7 in ADCSRA (ADC Enable) p 223				
	LDI TMP1, 0x00 								
	OUT ADMUX, TMP1								; clear ADMUX
	RJMP EXIT_PARSE

; write 3rd message onto LCD
TASK_9:
	CALL DISPLAY_LCD  							
	CALL SEND_MENU
	RJMP EXIT_PARSE

; clear LCD
TASK_10:
	LDI TMP1, 0x01								; clear the LCD
	CALL WRITE_INSTRUC
	RJMP EXIT_PARSE

; reset the microcontroller
TASK_11:
	LDI TMP1, 0b00001000						; Set watch dog enable
	OUT WDTCR, TMP1								; start watchdog timer. When interrupt occurs, jumps to Start
	RJMP EXIT_PARSE

; turn motor routine
TURN_MOTOR:
	SBRS MOT_STAT, 1							; if the second bit in mot_stat is set it is on so do move
	RET
	SBRC MOT_STAT, 0							; Motor on, so check which direction to turn the motor
	RJMP CLOCKWISE
	RJMP ANTI_CLOCKWISE 						; not 0, so 1, so move anti clockwise

CLOCKWISE:
   	LSL MASK 
	CPI MASK, 0x00
	BRNE DO_TURN
	LDI MASK, 0b00010000 						; wrap around 
	RJMP DO_TURN

ANTI_CLOCKWISE:
	LSR MASK
	CPI MASK, 0b00001000						; wrap around MSB
	BRNE DO_TURN
	LDI MASK, 0b10000000

DO_TURN:
	OUT PORTD, MASK
	RET

; called from START
; transfers messages from EEPROM into RAM
STORE_IN_RAM:
	LDI XL, LOW(EE_LOOKUP)									; loads address of EE_Lookup (defined above)
	LDI XH,	HIGH(EE_LOOKUP)
	LDI YL, LOW(MESSAGE_1)									; loads address of msg 1
	LDI YH,	HIGH(MESSAGE_1)
	CALL READ_BYTE											; put message into RAM
	LDI YL, LOW(MESSAGE_2)
	LDI YH,	HIGH(MESSAGE_2)
	CALL READ_BYTE
	LDI YL, LOW(MESSAGE_3)
	LDI YH,	HIGH(MESSAGE_3)

READ_BYTE:
	SBIC EECR, EEWE											; wait to make sure there is no active write
	RJMP READ_BYTE											; write still in progress so loop back to start
	OUT EEARH, XH											; record EEPROM address where the next read is going to come from
	OUT EEARL, XL
	SBI EECR, EERE											; enable read operation from EEPROM
	IN TMP2, EEDR											; store data i.e. 0x10 -> actual value you're reading in
	ST Y+, TMP2												; store in RAM and post-increment so the next piece of data can be stored in the next free location in RAM
	ADIW XL, 1												; moves to the next word that is stored in the EEPROM
	CPI TMP2, 0x00
	BREQ EXIT_READ
	RJMP READ_BYTE
EXIT_READ:
	RET

DISPLAY_LCD:
	LDI TMP1, 0x80 ; 0b10000000						 		; bit 7 sets DDRAM address. Bit6:0 location to start writing chars
	CALL WRITE_CHAR											; routine in lcd.asm
	CALL INIT_LCD											; calls a routine inside lcd.asm



READ_FROM_RAM:
	LDI XL, LOW(MESSAGE_3)									; get the address from RAM (hi byte)
	LDI XH, HIGH(MESSAGE_3)									; get the address from RAM (lo byte)
	MOV COUNT, ZERO											; reset count to 0
LOOP:
	SBRC COUNT, 4											; if 16 characters across
	RCALL MOVE_TO_NEXT_LINE									;	go to next line..
	LD TMP1, X												; else continue on current line
	CPI TMP1, 0x00											; if reached end of message, exit
	BREQ EXIT
	CALL WRITE_CHAR											; write the character
	INC COUNT												; increment number of characters written
	LDI TMP1, 0x01											; used to increment x register
	ADD XL, TMP1											; get the next character
	ADC XH, ZERO
	RJMP LOOP
EXIT:
	RET

MOVE_TO_NEXT_LINE:
	MOV COUNT, ZERO
	LDI TMP1, 0xc0	; 0b11000000 							  (sets DDRAM address and move by 16 pts)
	CALL WRITE_INSTRUC	
	RET									

; stored in program memory (CSEG)
TEXT_MENU:
.db 0x1b,"[2J", 0x1b, "[H", "PROJECT TASKS: ",0x0d,0x0a, \
  "---------------", 0x0d, 0x0a, \
  "1) Single step clockwise", 0x0d,0x0a, \
  "2) Single step anti-clockwise", 0x0d,0x0a, \
  "3) 180 clockwise", 0x0d,0x0a, \
  "4) 180 anti-clockwise", 0x0d,0x0a, \
  "5) Disable Stepper", 0x0d,0x0a, \
  "6) Enable Stepper", 0x0d,0x0a, \
  "7) Start ADC PWM Task", 0x0d,0x0a, \
  "8) Stop ADC PWM Task", 0x0d,0x0a, \
  "9) Print 3rd stored message to LCD", 0x0d,0x0a, \
  "10) Clear LCD",0x0d,0x0a, \
  "11) Reset Micro-controller",0x0d,0x0a,0x00

.include "LCD.asm"
