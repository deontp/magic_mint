/*
Author: Dylan Smith
Date: 17 March 2016
Program that creates an md5 hash, flips it and then feeds it into a sha 256 crypto algorithm.
Each password from the dictionary fed into the program and then compared to hash table to see if a match.
Outputs matches as necessary
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.IO;

namespace md5_flip_sha256
{
    class Program
    {
        static void Main(string[] args)
        {

            // read in dictionary file:
            const string dict = "C:\\Users\\g13s0714\\Desktop\\dict\\directory-list-lowercase-2.3-big.txt";
            List <string> dictLines = new List<string>();
            using (StreamReader r = new StreamReader(dict))
            {
                string line;
                while ((line = r.ReadLine()) != null) dictLines.Add(line);
            }
            // read in hash file: 
            const string hashTable = "C:\\Users\\g13s0714\\Desktop\\CS Honours\\Information Security\\Hashes\\md5_flip_sha256_cracked\\RemovedEntries8.hash";
            List<string> hashLines = new List<string>();
            using (StreamReader r = new StreamReader(hashTable))
            {
                string line;
                while ((line = r.ReadLine()) != null) hashLines.Add(line);
            }

            List<string> matches = Compare(dictLines, hashLines);
            OutputResults(matches, hashLines);
            Console.WriteLine("done...");
            Console.ReadLine();
            #region test code
            //// create the text that will be hashed
            //string source = "Dylan";
            //MD5 md5hash = MD5.Create();
            //// get the hashed value back
            //string hash = GetMd5Hash(md5hash, source);
            //Console.WriteLine("md5 hash: " + hash);
            //// reverse the md5 hash
            //Console.WriteLine("reversed md5 hash:" + ReverseMd5(hash));
            //string reversed = ReverseMd5(hash);
            ////Console.ReadLine();
            ////feed to sha256
            //SHA256 mySHA256 = SHA256.Create();
            //string finalhash = GetSha256Hash(mySHA256, ReverseMd5(hash));
            //Console.WriteLine("final: " + finalhash);
            //Console.ReadLine();
            #endregion
        }

        private static void OutputResults(List<string> output, List<string> toRemove)
        {
            File.WriteAllLines("C:\\Users\\g13s0714\\Desktop\\CS Honours\\Information Security\\Hashes\\md5_flip_sha256_cracked\\md5_flip_sha256_cracked9.txt", output);
            File.WriteAllLines("C:\\Users\\g13s0714\\Desktop\\CS Honours\\Information Security\\Hashes\\md5_flip_sha256_cracked\\RemovedEntries9.hash", toRemove);
        }

        // check hashed password with hash table
        private static List<string> Compare(List<string> dictLines, List<string> hashLines)
        {
           List<string> matches = new List<string>();
            dictLines.Sort();
            hashLines.Sort();
           //for (int i = 0; i < dictLines.Count; i++)
           // {
           //     string result = GetValue(dictLines[i]);                                            // pass entry from dict into algorithm
           //     for (int j = 0; j < hashLines.Count; j++)
           //     {
           //         if (hashLines[j].Equals(result))
           //         {
           //             matches.Add("md5_flip_sha56:" + result + ":" + dictLines[i]);
           //             hashLines.Remove(result);
           //         }
           //     }
           // }
           // return matches;
           for (int i = 0; i < dictLines.Count; i++)
            {
                string result = GetValue(dictLines[i]);
                int index = hashLines.BinarySearch(result);
                if (index > 0)
                {
                    matches.Add("md5_flip_sha56:" + result + ":" + dictLines[i]);
                    hashLines.Remove(result);
                }
            }
            return matches;
        }

        private static string GetValue(string source)
        {
            string finalhash = string.Empty;
            using (MD5 md5hash = MD5.Create())
            {
                string hash = GetMd5Hash(md5hash, source);
                string rev = ReverseMd5(hash);
                using (SHA256 mySHA256 = SHA256.Create())
                {
                    finalhash = GetSha256Hash(mySHA256, rev);
                }
            }
            return finalhash;
        }

        private static string GetSha256Hash(SHA256 sha256hash, string input)
        {
            byte[] data = sha256hash.ComputeHash(Encoding.UTF8.GetBytes(input));
            StringBuilder sBuilder = new StringBuilder();
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string.
            return sBuilder.ToString();
        }

       private static string GetMd5Hash(MD5 md5Hash, string input)
        {
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));

            StringBuilder sBuilder = new StringBuilder();

            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            return sBuilder.ToString();
        }

       private static string ReverseMd5(string input)
        {
            StringBuilder s = new StringBuilder();
            char[] md5ToArray = input.ToCharArray();
            Array.Reverse(md5ToArray);
            string md5Reversed = string.Empty;
            for (int i = 0; i < md5ToArray.Length; i++) s.Append(md5ToArray[i]);
            md5Reversed = s.ToString();
            return md5Reversed;
        } 
    }
}
