REM Author: Paul Sebeikin
REM Date: 19 March 2016
REM Multiple runs for the .dic files for SHA1 algorithms.
cd "C:\infosecNew\hashcat\"
hashcat-cli64.exe -m 100 --remove -o C:\infosecNew\cracked\sha1_c.txt -p ":\n" C:\infosecNew\hashes\sha1.hash C:\infosecNew\dict\*.dic
pause