REM Author: Dylan Smith
REM Date: 18 March 2016
REM Multiple runs for the .dict files for MD5 algorithm.

cd "C:\Users\Dylan\Desktop\Uni Work\Honours\Information Security\hashcat-2.00"
hashcat-cli64.exe -m 0 --remove -o "C:\Users\Dylan\Desktop\hasjcattest\md5_c" -p ":\n" "\\zero\infosecNew\hashes\md5.hash" "\\zero\infosecNew\dict\*.dic"

pause
