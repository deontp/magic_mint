.include "m16def.inc"	

.def TMP1=R16		
.def TMP2=R17
.def TMP3=R18		
.def RXPOS=R19
.def ZERO=R20
.def MASK=R21
.def COUNT=R22
.def MOT_STAT=R23							; bit setup: 0=direction(1=cw, 0=acw), 1=on/off(1 on 0 off), 2=menu sent

.eseg
.org 0x000
EE_LOOKUP: .db "Message 1", 0x00,\
	"Message 2", 0x00,\
	"Message 3 ABCDEFGHI", 0x00,0x00

.dseg 
RXBUFFER: .byte 10 ; reserve 10bytes for the message received
;LCDbuffer: .byte 10 ; lcd time buffer
MESSAGE_1: .byte 20 ; used to store the message from eeprom
MESSAGE_2: .byte 20
MESSAGE_3: .byte 20

.cseg			
.org $000		;locate code at address $000
jmp START		; Jump to the START Label
.org URXCaddr
jmp URXC_ISR	
.org UTXCaddr
jmp UTXC_ISR
.org OVF0addr
jmp OFV0_ISR
.org ADCCaddr
rjmp ADC_ISR
 
.org $02A		;locate code past the interupt vectors

START: 	
	LDI TMP1, LOW(RAMEND)	                     ;initialise the stack pointer
	OUT SPL, TMP1
	LDI TMP1, HIGH(RAMEND)
	OUT SPH, TMP1
	CALL INIT_UART
	CALL SETUP_PORTS
	CALL SETUP_MOTOR
	CALL STORE_IN_RAM
	CALL SEND_MENU
	LDI TMP1, 0x00
	MOV ZERO, TMP1
	SEI

MAIN_LOOP:
	NOP
	NOP
	RJMP MAIN_LOOP

SETUP_PORTS:
	LDI TMP1, 0b11111111	
	OUT DDRA, TMP1								; set all bits in PORTA to output mode.
	LDI TMP1, 0b00000001
	OUT PORTA, TMP1								; all LED's initially off		
	RET

SETUP_LCD:
	;LDI TMP1, 0X80
	;CALL WRITE_INSTRUC
	;CALL DELAY
	LDI TMP1, 0b00111000						; double line 
	CALL WRITE_INSTRUC
	;LDI TMP1, 0b00000101
	;CALL WRITE_INSTRUC
	LDI TMP1, 0b00011100
	CALL WRITE_INSTRUC
	RET

SETUP_MOTOR:	
	LDI TMP1, 0b11110000
	IN TMP2, DDRD
	OR TMP2, TMP1
	OUT DDRD, TMP2								; setting PD
	LDI MASK, 0b10000000
	OUT PORTD, MASK
	ORI MOT_STAT, 0b00000010					; set the motor status to on
	RET

SETUP_TIMER_0:									; setting the timer over flow to turn the motor 180 degrees with prescaller of 1024
	LDI TMP1, 0b00000001
	OUT TIMSK, TMP1
	MOV TMP1, ZERO
	OUT TCNT0, TMP1	
	LDI TMP1, 0b00000100						; start the timer with a prescaler of 254
	OUT TCCR0, TMP1
	RET
SEND_MENU:
    ; Z is reserved for pointing to next char
	; Z register is used specifically for program memory because LPM instruction
	; only works with Z register.	
    LDI ZL, LOW(2*TEXT_MENU)
    LDI ZH, HIGH(2*TEXT_MENU)
	LPM TMP1, Z+

	;MOV TMP3, MOT_STAT
	;ANDI TMP3, 0b00000100
	;CPI TMP3, 0b00000100
	;BREQ DONE
	OUT UDR, TMP1    	
	;ORI MOT_STAT, 0b00000100
;DONE:
	RET

INIT_UART:
;set baud rate (9600,8,n,2)
 	LDI TMP1, 51				; page 173 in ATMega Datasheet
	OUT UBRRH, ZERO				; set to 0 beause we don't use the upper 8 bits 
	OUT	UBRRL, TMP1				; set to 51 for the USART config
;set rx and tx enable
	SBI UCSRB, RXEN
	SBI UCSRB, TXEN
; enable uart interrupts
	SBI UCSRB, RXCIE
    SBI UCSRB, TXCIE
	RET

DELAY_MENU:
		PUSH TMP3
		PUSH TMP2
		PUSH TMP1
		SER TMP1		; TMP1=0xff
Del1:	SER TMP2		; TMP2=0xff
Del2:	LDI TMP3, 20 	; TMP=10 decimal
Del3:	DEC TMP3		; decrement TMP3
		BRNE Del3
		DEC TMP2
		BRNE Del2
		DEC TMP1
		BRNE Del1
		POP TMP1
		POP TMP2
		POP TMP3
		RET

; Interrupt code for when UDR empty
; Code for TX complete 
UTXC_ISR:
	LPM TMP1, Z+
	CPI TMP1, 0x00	 
	BREQ EXIT_TX_ISR
	OUT UDR, TMP1
	RETI

EXIT_TX_ISR:
;disable txcomplete int
	CBI UCSRB, TXCIE							; no more characters will be sent via USART
	RETI

; Code for RX complete
URXC_ISR:
    IN TMP1, UDR
    LDI XL,LOW(RXBUFFER)				; acquires the position in RAM where message to be stored
    LDI XH,HIGH(RXBUFFER)
	ADD XL, RXPOS
	ADC XH, ZERO
    ST X, TMP1
    CPI TMP1, '.'
	BREQ PARSE_MESSAGE
	INC RXPOS
	RETI

OFV0_ISR:
	; spills registers to stack	
	PUSH TMP1
	PUSH TMP2	
	IN TMP1, SREG
	PUSH TMP1

	CPI COUNT, 100
	BREQ STOP_TIMER_0
   	RCALL TURN_MOTOR
	INC COUNT

	; restore from the stack
	POP TMP1
	OUT SREG, TMP1	
	POP TMP2
	POP TMP1
	RETI
STOP_TIMER_0:
	OUT TCCR0, ZERO
	OUT TIMSK, ZERO

	; restore from the stack
	POP TMP1
	OUT SREG, TMP1	
	POP TMP2
	POP TMP1
	RETI

ADC_ISR:
	; spill registers to stack
	PUSH TMP1
	PUSH TMP2	
	IN TMP1, SREG
	PUSH TMP1

	;IN TMP1, 0b00000000						; read value in from ADC
	IN TMP1, ADCH
	LSR TMP1
	LSR TMP1
	LSR TMP1
	LSL TMP1
	;IN TMP2, PINA
	;OR TMP1, TMP2
	OUT PORTA, TMP1

	POP TMP1
	OUT SREG, TMP1
	POP TMP2
	POP TMP1

	RETI

PARSE_MESSAGE:
    LDI XL, LOW(RXBUFFER)
    LDI XH, HIGH(RXBUFFER)
	LD TMP1, X+													; increment X to point to the next byte to check if we are working with double digits
	CPI TMP1, '1'
	BREQ TASK_1
	CPI TMP1, '2'
	BREQ TASK_2
	CPI TMP1, '3'
	BREQ TASK_3	
	CPI TMP1, '4'
	BREQ TASK_4
	CPI TMP1, '5'
	BREQ TASK_5
	CPI TMP1, '6'
	BREQ TASK_6
	CPI TMP1, '7'
	BREQ TASK_7
	CPI TMP1, '8'
	BREQ TASK_8
	CPI TMP1, '9'
	BREQ TASK_9	

EXIT_PARSE:
    MOV RXPOS, ZERO
	RETI

TASK_1:											; single step clockwise		
	LD TMP1, X
	CPI TMP1, '0'								; checks if we have double digits and decides where to go
	BREQ TASK_10
	CPI TMP1, '1'
	BREQ TASK_11
	ORI MOT_STAT, 0b00000001					; set direction to clockwise
	RCALL TURN_MOTOR
	RJMP EXIT_PARSE
TASK_2:											; single step anti-clockwise
	ANDI MOT_STAT, 0b11111110					; set direction to anti-clockwise
	RCALL TURN_MOTOR
	RJMP EXIT_PARSE
TASK_3:											; turn 180 clockwise 
	SBRS MOT_STAT, 1							; if the second bit in mot_stat is set it is on so do move
	RJMP EXIT_PARSE
	CALL SEND_MENU				
	MOV COUNT, ZERO
	ORI MOT_STAT, 0b00000001					; set direction to clockwise	
	CALL SETUP_TIMER_0	
	RJMP EXIT_PARSE
TASK_4:											; turn 180 anti-clockwise 
	SBRS MOT_STAT, 1							; if the second bit in mot_stat is set it is on so do move
	RJMP EXIT_PARSE
	CALL SEND_MENU
	MOV COUNT, ZERO
	ANDI MOT_STAT, 0b11111110					; set direction to anti-clockwise		
	CALL SETUP_TIMER_0	
	RJMP EXIT_PARSE

TASK_5:											; disable stepper motor	
	ANDI MOT_STAT, 0b11111101					; turn stepper motor off by clearing bit 1 in MOT_STAT
	LDI MASK, 0x00
	OUT PORTD, MASK
	RJMP EXIT_PARSE		
TASK_6:											; enable stepper motor	
	ORI MOT_STAT, 0b00000010	
	LDI MASK, 0b10000000
	OUT PORTD, MASK
	RJMP EXIT_PARSE
TASK_7:											; start ADC PWM
	LDI TMP1, 0b01100000
	OUT ADMUX, TMP1
	LDI TMP1, 0b11111111
	OUT ADCSRA, TMP1
	RJMP EXIT_PARSE
TASK_8:
	CBI ADCSRA, 7								; disables the ADC	
	LDI TMP1, 0x00
	OUT ADMUX, TMP1								; clear ADMUX
	RJMP EXIT_PARSE
TASK_9:		
	CALL DISPLAY_LCD		
	CALL SEND_MENU
	RJMP EXIT_PARSE
TASK_10:
	LDI TMP1, 0x01								; clear the terminal
	CALL WRITE_INSTRUC
	RJMP EXIT_PARSE
TASK_11:	
	LDI TMP1, 0b00001000						; Set watch dog enable
	OUT WDTCR, TMP1								; start watchdog timer. When interrupt occurs, jumps to Start
	RJMP EXIT_PARSE

;CLEAR_TERMINAL:
;	LDI ZL, LOW(2*CLEAR)						; load address in program memory where 0x1b is stored
 ;   LDI ZH, HIGH(2*CLEAR)
;	LPM TMP1, Z+								; load from Z register
;	SBI UCSRB, TXCIE							; enable transmission complete interrupt
;	OUT UDR, TMP1								; send char 0x1b	
;	RET

TURN_MOTOR:
	SBRS MOT_STAT, 1							; if the second bit in mot_stat is set it is on so do move
	RET
	
	SBRC MOT_STAT, 0							; checking which direction to turn the motor
	RJMP CLOCKWISE
	RJMP ANTI_CLOCKWISE

CLOCKWISE:
   	LSL MASK
	CPI MASK, 0x00
	BRNE DO_TURN
	LDI MASK, 0b00010000
	RJMP DO_TURN

ANTI_CLOCKWISE:
	LSR MASK
	CPI MASK, 0b00001000						; wrap around MSB
	BRNE DO_TURN
	LDI MASK, 0b10000000
  	
DO_TURN:
	OUT PORTD, MASK
	RET
;######################################################
STORE_IN_RAM:
	LDI XL, LOW(EE_LOOKUP)
	LDI XH,	HIGH(EE_LOOKUP)
	LDI YL, LOW(MESSAGE_1)
	LDI YH,	HIGH(MESSAGE_1)
	CALL READ_BYTE
	LDI YL, LOW(MESSAGE_2)
	LDI YH,	HIGH(MESSAGE_2)
	CALL READ_BYTE
	LDI YL, LOW(MESSAGE_3)
	LDI YH,	HIGH(MESSAGE_3)

READ_BYTE:
	SBIC EECR, EEWE											; wait to make sure there is no active write
	RJMP READ_BYTE											; write still in progress so loop back to start
	OUT EEARH, XH											; record EEPROM address where the next read is going to come from
	OUT EEARL, XL
	SBI EECR, EERE											; enable read operation from EEPROM
	IN TMP2, EEDR											; store data i.e. 0x10		
	ST Y+, TMP2												; store in RAM and post-increment so the next piece of data can be stored in the next free location in RAM
	ADIW XL, 1												; moves to the next word that is stored in the EEPROM		
	CPI TMP2, 0x00
	BREQ EXIT_READ
	RJMP READ_BYTE
EXIT_READ:	
	RET

DISPLAY_LCD:	
	CALL INIT_LCD											; calls a routine inside lcd.asm
	CALL SETUP_LCD											; sends all the instructions	

READ_FROM_RAM:
	LDI XL, LOW(MESSAGE_3)
	LDI XH, HIGH(MESSAGE_3)
LOOP:	
	LD TMP1, X
	CPI TMP1, 0x00
	BREQ EXIT
	CALL WRITE_CHAR	
	LDI TMP1, 0x01
	ADD XL, TMP1
	ADC XH, ZERO
	RJMP LOOP
EXIT:
	RET
;######################################################
; stored in program memory (CSEG)
TEXT_MENU: 
.db 0x1b,"[2J", 0x1b, "[H", "PROJECT TASKS: ",0x0d,0x0a, \
  "---------------", 0x0d, 0x0a, \
  "1) Single step clockwise", 0x0d,0x0a, \
  "2) Single step anti-clockwise", 0x0d,0x0a, \
  "3) 180 clockwise", 0x0d,0x0a, \
  "4) 180 anti-clockwise", 0x0d,0x0a, \
  "5) Disable Stepper", 0x0d,0x0a, \
  "6) Enable Stepper", 0x0d,0x0a, \
  "7) Start ADC PWM Task", 0x0d,0x0a, \
  "8) Stop ADC PWM Task", 0x0d,0x0a, \
  "9) Print 3rd stored message to LCD", 0x0d,0x0a, \
  "10) Clear LCD",0x0d,0x0a, \
  "11) Reset Micro-controller",0x0d,0x0a,0x00

;CLEAR: .db 									; ESC, clear terminal, ESC, go home

.include "LCD.asm"
