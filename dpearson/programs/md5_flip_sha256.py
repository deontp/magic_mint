import hashlib
import os

def fileToList(filename): # takes a file and puts it into an array
    temp = []
    temp = open(filename, 'r').read().split('\n')
    return temp

def flipString(txt):
    temp =''
    count = len(txt) - 1
    while count >= 0:
        temp += txt[count]
        count -= 1
    return temp

def doHash (plainText):
    md5Hash = hashlib.md5(b'%s' % (plainText,))
    flipHash = flipString(md5Hash.hexdigest())
    sha256Hash = hashlib.sha256(b'%s' % (flipHash,))
    return sha256Hash.hexdigest()

def outputResults(hashResult,plainText):
    with open('/home/deonp/GitHub/magic_mint/dpearson/cracked/md5_flip_sha256_3.results', 'a') as f:
        f.write('%s:%s:%s\n' % ('md5_flip_sha256',hashResult,plainText))

def hashDictionary(inputList):
    temp = {}
    for item in inputList:
        if item != '':
                temp[doHash(item)] = item
    return temp

if __name__ == '__main__':
    count = 0
    foundHashes = []
    hashList = fileToList('/home/deonp/GitHub/magic_mint/dpearson/og_hashes/md5_flip_sha256.hash')
    dictList = fileToList('/home/deonp/Downloads/dictionaries/realhuman_phill.txt')
    dictList.pop()
    for i in dictList:
        temp = doHash(i)
        if temp in hashList:
            print '%s:%s\n' % (temp, i)
            outputResults(temp, i)
