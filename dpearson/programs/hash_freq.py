import os
import my_module

def cleanHashes(hashes):
    temp = {}
    for key in hashes:
        if key != '':
            if temp.has_key(key):
                temp[key] += 1
            else:
                temp[key] = 1
    return temp

def outputUniqueHashes(fileName, dict):
    with open(fileName, 'w') as f:
        for l in dict:
            f.write('%s:%d\n' % (l,dict[l]))

if __name__ == '__main__':
    inp = '/home/deonp/GitHub/magic_mint/dpearson/og_hashes/md5_flip_sha256.hash'
    out = '/home/deonp/GitHub/magic_mint/dpearson/freq_count/md5_flip_sha256.hash'
    hashes = my_module.fileToList(inp)
    freqDictionary = cleanHashes(hashes)
    outputUniqueHashes(out, freqDictionary)
