def fileToList(filename): # takes a file and puts it into an array
    temp = []
    temp = open(filename, 'r').read().split('\n')
    return temp

def outputUniqueHashes(fileName, dict):
    with open(fileName, 'w') as f:
        for l in dict:
            f.write('%s:%s\n' % (l,dict[l]))
def fileToDictionary(filename):
    res = {}
    for i in fileToList(filename):
        if i != '':
            temp = i.split(':')
            res[temp[0]] = temp[1]
    return res
