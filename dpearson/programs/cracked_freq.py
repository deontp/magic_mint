import os
import my_module

#def doWork(hashesFreqDictionary, crackedHashDictionary):
#    res = {}
#    for item in crackedHashDictionary:
#        if item != '' :
#            res[item] = [crackedHashDictionary[item], hashesFreqDictionary[item]]
#    return res
def doWork(hashesFreqDictionary, crackedHashDictionary):
    res = {}
    for item in hashesFreqDictionary:
        if item != '' :
            if item not in crackedHashDictionary:
                temp = 1
                # res[item] = ['none', hashesFreqDictionary[item]]
            else:
                res[item] = [crackedHashDictionary[item], hashesFreqDictionary[item]]
    return res

def writeToFile(fileName, dict):
    with open(fileName, 'w') as f:
        for l in dict:
            temp = dict[l]
            f.write('%s:%s:%s\n' % (l,temp[0],temp[1]))

if __name__ == '__main__':
    hashFreq = '/home/deonp/GitHub/magic_mint/dpearson/freq_count/md5_flip_sha256.hash'
    cHash= '/home/deonp/GitHub/magic_mint/dpearson/cracked/md5_flip_sha256.out'
    out = '/home/deonp/GitHub/magic_mint/dpearson/reports/md5_flip_sha256.report'

    hashesFreqD = my_module.fileToDictionary(hashFreq)
    cHashD = my_module.fileToDictionary(cHash)
    res = doWork(hashesFreqD, cHashD)
    writeToFile(out, res)
