import os.path

def fileToDict(filename, dic): # takes a file and puts it into an array
    #print dic
    with open(filename, 'r') as f:
        for l in f :
            temp = l.split(':')
            if len(temp) == 3 and temp[1] not in dic.keys():
                dic[temp[1]] = temp[2].strip('\n')
    #print dic
    return dic

def outputResults(outputFile, dict): # puts the result of the cracked hases into a new array
    with open(outputFile , 'w') as f:
        for i in dict:
            f.write('%s:%s\n' % (i, dict[i]))

def getPreviousHases(filename):
    dic = {}
    with open(filename, 'r') as f:
        for l in f :
            temp = l.split(':')
            if len(temp) == 2 and temp[1] not in dic.keys():
                dic[temp[0]] = temp[1].strip('\n')
    #print dic
    return dic

if __name__ == '__main__':
    inputFile = raw_input('filename to trim> ')
    outputFile = 'hashes.' + raw_input('algorithm> ')
    #inputFile = 'test.main'
    #outputFile = 'hashes.md5'
    tempDict = {}
    if os.path.isfile(outputFile):
        tempDict = getPreviousHases(outputFile)
    #print tempDict
    tempDict = fileToDict(inputFile, tempDict)
    #print tempDict
    outputResults(outputFile, tempDict)
